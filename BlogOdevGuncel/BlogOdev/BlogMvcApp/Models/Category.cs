﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogMvcApp.Models
{
    public class Category
    {
        public int Id { get; set; }

       
        [StringLength(maximumLength:10,ErrorMessage ="En fazla 10 karakter girilebilir!")]
        public string KategoriAdi { get; set; }

        public List<Blog> Bloglar { get; set; }
    }
}