﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BlogMvcApp.Controllers
{
    public class MultiLanguageController : Controller
    {
        // GET: MultiLanguage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Change(string lang)
        {
            if (Session["lang"] == null)
                Session["lang"] = "en";
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Session
                ["lang"].ToString());
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Session
                ["lang"].ToString());
       
            return View("Index");
        }
    }
}