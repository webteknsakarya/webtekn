﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(web_odevi.Startup))]
namespace web_odevi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
