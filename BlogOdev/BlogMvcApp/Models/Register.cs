﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogMvcApp.Models
{
    public class Register
    {
        [Required]
        [DisplayName("Adınız")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Soyadınız")]
        public string SurName { get; set; }

        [Required]
        [DisplayName("Kullanıcı Adınız")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Emailiniz")]
        [EmailAddress(ErrorMessage="Lütfen doğru bir eposta adresi giriniz...")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Şifreniz")]
        public string Password { get; set; }

        [Required]
        [DisplayName("Şire Tekrar")]
        [Compare("Password",ErrorMessage="Şifreleriniz uyuşmuyor.")]
        public string RePassword { get; set; }
        
    }
}